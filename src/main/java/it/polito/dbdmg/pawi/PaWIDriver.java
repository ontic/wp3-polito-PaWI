/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polito.dbdmg.pawi;

import static java.io.File.separator;
import static org.apache.hadoop.filecache.DistributedCache.addCacheFile;
import static org.apache.hadoop.mapreduce.lib.input.FileInputFormat.setInputPaths;
import static ua.ac.be.fpm.bigfim.AprioriPhaseReducer.COUNTER_GROUPNAME;
import static ua.ac.be.fpm.bigfim.AprioriPhaseReducer.COUNTER_NRLARGEPREFIXGROUPS;
import static ua.ac.be.fpm.hadoop.util.SplitByKTextInputFormat.NUMBER_OF_CHUNKS;
import static ua.ac.be.fpm.hadoop.util.Tools.cleanDirs;
import static ua.ac.be.fpm.hadoop.util.Tools.prepareJob;
import static ua.ac.be.fpm.util.FIMOptions.DELIMITER_KEY;
import static ua.ac.be.fpm.util.FIMOptions.FILTER_KEY;
import static ua.ac.be.fpm.util.FIMOptions.REPRESENTATION_KEY;
import static ua.ac.be.fpm.util.FIMOptions.MIN_SUP_KEY;
import static ua.ac.be.fpm.util.FIMOptions.NUMBER_OF_LINES_KEY;
import static ua.ac.be.fpm.util.FIMOptions.NUMBER_OF_MAPPERS_KEY;
import static ua.ac.be.fpm.util.FIMOptions.OUTPUT_DIR_KEY;
import static ua.ac.be.fpm.util.FIMOptions.PREFIX_LENGTH_KEY;
import static ua.ac.be.fpm.eclat.EclatMinerMapperBase.stampaApriori;
import ua.ac.be.fpm.bigfim.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.ContentSummary;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import ua.ac.be.fpm.eclat.EclatMinerMapper;
import ua.ac.be.fpm.eclat.EclatMinerReducer;
import ua.ac.be.fpm.hadoop.util.FloatArrayWritable;
import ua.ac.be.fpm.hadoop.util.FloatMatrixWritable;
import ua.ac.be.fpm.hadoop.util.NoSplitSequenceFileInputFormat;
import ua.ac.be.fpm.hadoop.util.SplitByKTextInputFormat;
import ua.ac.be.fpm.util.FIMOptions;

/**
 * Driver class of the PaWI miner. 
 * Input mandatory parameters/options
 * -i filename (name of the file containing the set of weighted transactions to analyze
 * -o output folder (frequent weighted itemsets will be store in output folder/fis)
 * -s absolute minimum support threshold (an integer positive number representing the absolute minimum support threshold)
 * -m number mappers (the number of mappers to be instanced)
 * -p prefix length  (size of the prefixes mined by the apriori-based phase of the algorithm)
 */

public class PaWIDriver implements Tool {
  
  private static final String OFis = "fis";
  public static final String rExt = "-r-00000";
  
  public static void main(String[] args) throws Exception {
    ToolRunner.run(new PaWIDriver(), args);
  }
  
  @Override
  public int run(String[] args) throws Exception {
    FIMOptions opt = new FIMOptions();
    // control that the mandatory parameters are set and prefixLenght != -1
    if (!opt.parseOptions(args)) {
      opt.printHelp();
      return -1;
    }
    
    // Clean the Hadoop file system deleting files if they exist
    cleanDirs(new String[] {opt.outputDir});
    // save the start time of the project
    long start = System.currentTimeMillis();
    
    // start the 1st map-reduce and return the value of phase(=prefixLenght)
    int phase = startAprioriPhase(opt);
    
    // assign the prefixLenght insert on input
    int prefixSize = opt.prefixLength;
    if(phase == prefixSize){
    	// start the 2nd map-reduce
        startCreatePrefixGroups(opt, phase);
        
     // directory containing the output of 2nd map-reduce
        String inputFilesDir = opt.outputDir + separator + "pg";
        Path ptc=new Path(inputFilesDir);
       	FileSystem fsc = FileSystem.get(new Configuration());
       	//store in children the list of files present in inputFilesDir
       	RemoteIterator<LocatedFileStatus> children = fsc.listFiles(ptc, true);
       	// initialize the counter for "bucket" files
       	int numBucketFiles = 0;
       	// if the directory contain files
       	if (children != null) {
       		while(children.hasNext()){
       			children.next();
       			Path p = children.next().getPath();
    		    // returns the filename or directory name if directory
    		    String name = p.getName();
    		    // search if there are present files starting with "bucket" (input for the mapper)
       			if(name.startsWith("bucket")){
       				numBucketFiles++;
       			}
       		}
       		// if there are present "bucket" files start mining with Eclat
       		if(numBucketFiles > 0){
       			startMining(opt);
       		} else {
       			// otherwise print only the result of Apriori phase (no input files for mining)
       			printAprioriResult(opt);
       		}
        } 
    } else {
        	// otherwise print only the result of Apriori phase (no input files for mining)
   			printAprioriResult(opt);
    }
    
    
    //  save the end time of the project
    long end = System.currentTimeMillis();
    
    // print the time execution of the project
    System.out.println("[PaWI]: Total time: " + (end - start) / 1000 + "s");
    return 1;
  }
  
  /**
   * Set the input parameter in the configuration variable
   */
  private static void setConfigurationValues(Configuration conf, FIMOptions opt) {
    conf.set(DELIMITER_KEY, opt.delimiter);
    // Add filter option
    conf.set(FILTER_KEY, opt.filter);
    // Add representation option
    conf.set(REPRESENTATION_KEY, opt.representation);
    // Modified the type from int to float
    conf.setFloat(MIN_SUP_KEY, opt.minSup);
    conf.setInt(NUMBER_OF_MAPPERS_KEY, opt.nrMappers);
    conf.setInt(NUMBER_OF_CHUNKS, opt.nrMappers);
    conf.setInt(PREFIX_LENGTH_KEY, opt.prefixLength);
    conf.setStrings(OUTPUT_DIR_KEY, opt.outputDir);
  }
  
  private static void runJob(Job job, String jobName) throws ClassNotFoundException, IOException, InterruptedException {
	// save the start time of the job
	long start = System.currentTimeMillis();
	// submit the job at cluster and wait the end
    job.waitForCompletion(true);
    // save the end time of the job
    long end = System.currentTimeMillis();
    
    // print the time execution of the job
    System.out.println("Job " + jobName + " took " + (end - start) / 1000 + "s");
  }
  
  
  /**
   * 1st map-reduce
   */
  protected int startAprioriPhase(FIMOptions opt) throws IOException, InterruptedException, ClassNotFoundException,
      URISyntaxException {
	
	long nrLines = -1;
	// assign the prefixLenght insert on input
    int prefixSize = opt.prefixLength;
    int i = 1;
    boolean run = true;
    while (run) {
      String outputDir = opt.outputDir + separator + "ap" + i;
      // save the name of the file with result of the previous apriori phase
      String cacheFile = opt.outputDir + separator + "ap" + (i - 1) + separator + "part-r-00000";
      System.out.println("[AprioriPhase]: Phase: " + i + " input: " + opt.inputFile + ", output: " + opt.outputDir);
      
      /**
       * Create a job setting parameters
       * PARAMETRI:
       * 1)inputFile	2)outputDir		3)inputFormat		4)Mapper		5)MapperKey
       * 6)MapperValue		7)Reducer		8)ReducerKey		9)ReducerValue		10)outputFormat
       */
      Job job = prepareJob(new Path(opt.inputFile), new Path(outputDir), SplitByKTextInputFormat.class,
          AprioriPhaseMapper.class, Text.class, FloatWritable.class, AprioriPhaseReducer.class, Text.class,
          FloatWritable.class, TextOutputFormat.class);
      
      job.setJobName("Apriori Phase" + i);
      job.setJarByClass(PaWIDriver.class);
      
      job.setNumReduceTasks(1);
      
      Configuration conf = job.getConfiguration();
      // set to conf the values of opt (the values passed from command line)
      setConfigurationValues(conf, opt);
      // if is the 1st time is surely -1
      if (nrLines != -1) {
        conf.setLong(NUMBER_OF_LINES_KEY, nrLines);
      }
      
      // add file in cache if it is not the 1st phase
      if (i > 1) {
    	// if "mapred.cache.files" is null set the URI, if not add it
        addCacheFile(new URI(cacheFile), conf);
      }
      
      // start job
      runJob(job, "Apriori Phase " + i);		    
      
      // end the "run" cicle if i = prefixSize
      if (prefixSize <= i) {
        run = false;
        if (prefixSize < i) {
          System.out.println("[AprioriPhase]: Prefix group length updated! Now " + (i) + " instead of " + prefixSize);
        }
      }
      // if apriori phase don't return itemset exit from the cycle)
      String aprioriFile = outputDir + "/part-r-00000";
      //Configuration config = new Configuration();
      Path path = new Path(aprioriFile);
      FileSystem hdfs = path.getFileSystem(conf);
      ContentSummary cSummary = hdfs.getContentSummary(path);
      long size = cSummary.getLength();
      float kSize = (float)size/1000;
      System.out.println("Apriori file size: " + kSize + " KB");
      if(size == 0){
    	  break;
      }
      
      i++;
    }
    // return the phase (#Apriori_cicle)
    return i - 1;
  }
  
  /**
   * 2nd map-reduce
   */
  private void startCreatePrefixGroups(FIMOptions opt, int phase) throws IOException, ClassNotFoundException,
      InterruptedException, URISyntaxException {
	// save the name of the file with result of last apriori phase
    String cacheFile = opt.outputDir + separator + "ap" + phase + separator + "part-r-00000";
    String outputFile = opt.outputDir + separator + "pg";
    System.out.println("[CreatePrefixGroups]: input: " + opt.inputFile + ", output: " + opt.outputDir);
    
    /**
     * Create a job setting parameters
     * PARAMETERS:
     * 1)inputFile	2)outputDir		3)inputFormat		4)Mapper		5)MapperKey
     * 6)MapperValue		7)Reducer		8)ReducerKey		9)ReducerValue		10)outputFormat
     */
    Job job = prepareJob(new Path(opt.inputFile), new Path(outputFile), SplitByKTextInputFormat.class,
        ComputeTidListMapper.class, Text.class, FloatArrayWritable.class, ComputeTidListReducer.class,
        FloatArrayWritable.class, FloatMatrixWritable.class, SequenceFileOutputFormat.class);
    
    job.setJobName("Create Prefix Groups");
    job.setJarByClass(PaWIDriver.class);
    job.setNumReduceTasks(1);
    
    Configuration conf = job.getConfiguration();
    // set to conf the values of opt (the values passed from command line)
    setConfigurationValues(conf, opt);
    // set the phase at PREFIX_LENGHT_KEY
    conf.setInt(PREFIX_LENGTH_KEY, phase);
    
    // add the result's file of last apriori phase
    addCacheFile(new URI(cacheFile), job.getConfiguration());
    
    // start job
    runJob(job, "Prefix Creation");
  }
  
  /**
   * 3rd map-reduce
   */
  private void startMining(FIMOptions opt) throws IOException, ClassNotFoundException, InterruptedException {
    String inputFilesDir = opt.outputDir + separator + "pg" + separator;
    String outputFile = opt.outputDir + separator + OFis;
    System.out.println("[StartMining]: input: " + inputFilesDir + ", output: " + outputFile);
    
    /**
     * Create a job setting parameters
     * PARAMETERS:
     * 1)inputFile	2)outputDir		3)inputFormat		4)Mapper		5)MapperKey
     * 6)MapperValue		7)Reducer		8)ReducerKey		9)ReducerValue		10)outputFormat
     */
    Job job = prepareJob(new Path(inputFilesDir), new Path(outputFile), NoSplitSequenceFileInputFormat.class,
    		        EclatMinerMapper.class, FloatWritable.class, Text.class, EclatMinerReducer.class, FloatWritable.class, Text.class,
    		        TextOutputFormat.class);
    		    
    job.setJobName("Start Mining");
    job.setJarByClass(PaWIDriver.class);
    job.setNumReduceTasks(1);
    		    
    Configuration conf = job.getConfiguration();
    // set to conf the values of opt (the values passed from command line)
    setConfigurationValues(conf, opt);
    		    
    List<Path> inputPaths = new ArrayList<Path>();
        
    // return all the files that match with parameter of globStatus in the FileSystem configured 
    FileStatus[] listStatus = FileSystem.get(conf).globStatus(new Path(inputFilesDir + "bucket*"));
     
    for (FileStatus fstat : listStatus) {
    	inputPaths.add(fstat.getPath());
    }
        	    
    // set the list like input for map-reduce job
    setInputPaths(job, inputPaths.toArray(new Path[inputPaths.size()]));
    
    // start job
    runJob(job, "Mining");
    
 }
  
  
  /**
   * 3rd map-reduce if don't have "bucket" files 
   */
  private void printAprioriResult(FIMOptions opt) throws IOException, ClassNotFoundException, InterruptedException {
	    //String inputFilesDir = opt.outputDir + separator + "ap1" + separator;
	    String outputFile = opt.outputDir + separator + OFis;
	    System.out.println("[PrintAprioriResult]: input: " + opt.inputFile + ", output: " + outputFile);
	    
	    /**
	     * Create a job setting parameters
	     * PARAMETERS:
	     * 1)inputFile	2)outputDir		3)inputFormat		4)Mapper		5)MapperKey
	     * 6)MapperValue		7)Reducer		8)ReducerKey		9)ReducerValue		10)outputFormat
	     */
	    Job job = prepareJob(new Path(opt.inputFile), new Path(outputFile), SplitByKTextInputFormat.class,
	    		        PrintAprioriMapper.class, FloatWritable.class, Text.class, EclatMinerReducer.class, FloatWritable.class, Text.class,
	    		        TextOutputFormat.class);
	    		    
	    job.setJobName("Start Print");
	    job.setJarByClass(PaWIDriver.class);
	    job.setNumReduceTasks(1);
	    		    
	    Configuration conf = job.getConfiguration();
	    // set to conf the values of opt (the values passed from command line)
	    setConfigurationValues(conf, opt);
	    
	    // start job
	    runJob(job, "Mining");
	    
	 }
   
  
  @Override
  public Configuration getConf() {
    return null;
  }
  
  @Override
  public void setConf(Configuration arg0) {}
}
