/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.eclat;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static java.util.Arrays.copyOf;
import static ua.ac.be.fpm.util.Tools.setDifference;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import ua.ac.be.fpm.eclat.util.Item;
import ua.ac.be.fpm.eclat.util.SetReporter;
import ua.ac.be.fpm.eclat.util.TidList;


/**
 * Eclat miner implementation for mining closed itemsets. This is the depth-first frequent itemset generation algorithm
 * proposed by Zaki et al. "New Algorithms for Fast Discovery of Association Rules". This implementation starts with tid
 * list, but immediately switches to diffSets when computing candidates for the next level.
 */
public class EclatMiner {
  
  public static class AscendingItemComparator implements Comparator<Item> {
    @Override
    public int compare(Item o1, Item o2) {
      return Float.compare(o1.support, o2.support);
    }
  }
  
  private SetReporter reporter;
  private long maxSize = Long.MAX_VALUE;
  
  public void setSetReporter(SetReporter setReporter) {
    this.reporter = setReporter;
  }
  
  /**
   * Sets the maximum length of an itemset found by the miner.
   * 
   * @param maxSize
   *          the maximum length of an itemset
   */
  public void setMaxSize(int maxSize) {
    this.maxSize = maxSize;
  }
  
  /**
   * Mines the sub prefix tree for frequent itemsets. items do not have to be conditioned, instead they should contain
   * full TID's.
   * 
   * @param item
   *          Prefix of the tree to mine.
   * @param items
   *          List of items with their initial TID lists.
   * @param minSup
   *          Minimum support
   */
  
  public void mineRecByPruning(Item item, List<Item> items, float minSup, String representation) {
    
    ArrayList<Item> newItems = newArrayList();
    
    for (Iterator<Item> it = items.iterator(); it.hasNext();) {
      Item item_2 = it.next();
      if (item.id == item_2.id) {
        continue;
      }
      
      TidList condTids = setDifference(item.getTids(), item_2.getTids());
      
      float newSupport = item.support - condTids.size();
      if (newSupport >= minSup) {
        Item newItem = new Item(item_2.id, newSupport, condTids);
        newItems.add(newItem);
      }
    }
    if (newItems.size() > 0) {
      newItems.trimToSize();
      declatRec(new float[] {item.id}, newItems, minSup, false, representation);
    }
  }
  
  /**
   * Mines the sub prefix tree for frequent itemsets.
   * 
   * @param prefix
   *          Prefix of the tree to mine.
   * @param extensions
   *          List of items with their conditional TID lists. All of the items should be frequent extensions of the
   *          prefix, i.e., support of union of prefix and each item should be greater than or equal to minSup.
   * @param minSup
   *          Minimum support
 * @param wList 
 * @param representation 
   */
  // Add parameter representation
  public void mineRec(float[] prefix, List<Item> extensions, float minSup, String representation ) {
    declatRec(prefix, extensions, minSup, true, representation);
  }
  
  // Add parameter representation
  private void declatRec(float[] prefix, List<Item> items, float minSup, boolean tidLists, String representation) {
    Iterator<Item> it1 = items.iterator();

    // Mi preparo a creare un nuovo prefisso che e' come quello attuale esteso di uno (aggiunta di un nuovo item)	
    float[] newPrefix = copyOf(prefix, prefix.length + 1);
    // items was extensions

    // Provo a estende il prefisso attuale con tutti gli item ancora disponibili	
    for (int i = 0; i < items.size(); i++) {

      Item item1 = it1.next();

      float support = item1.support;

      // Estendo il prefisso con l'item attuale (item1). Il supporto e' pari a quello di item1 (il suo supporto e' gia' calcolato sul DB proiettato (penso)) 
      // put the id of the item in last position of the array
      newPrefix[newPrefix.length - 1] = item1.id;
      
  
      // Verifica se il nuovo pefisso ha dim minore della eventuale lunghezza massima imposta per l'estrazione degli itemset
      // e inoltre verifica se il prefisso nuovo puo' essere ulteriormente esteso con un altro item verificando se ci sono
      // ancora degli item in items (guarda se l'item attuale e' prima dell'ultima posizione di items oppure no)
      // Non ho capito a cosa serve vedere se il prefisso puo' essere ancora esteso	
      final boolean canBeExtended = newPrefix.length < maxSize && i < items.size() - 1;
      // if the itemset can be extended with other items
      if (canBeExtended) {
	// Il numero di nuovi items ce possono essere accodati per estendere l'itemset attuale e' pari alla dimensione di items meno 
	// il numero di item che ho gia' analizzato (i e' l'indice dell'item attuale e quindi l'ultimo che ho analizzato) 
        List<Item> newItems = newArrayListWithCapacity(items.size() - i);

	// Recupera la tidlist di item1 che e' quello che ho aggiunto al vecchio prefisso per formare quello nuovo
        TidList tids1 = item1.getTids();
        
        // compare with next items
        for (ListIterator<Item> it2 = items.listIterator(i + 1); it2.hasNext();) {
          // Recupera il prossimo item da considerare come estensione
          // e poi la sua tidlit
	  Item item2 = it2.next();
          TidList tids2 = item2.getTids();
          TidList condTids;

	  // Se la variabile tidlist passata in fase di invocazione e' true allora fa una cosa altrimenti l'altra. La variabile mi sembra essere 
	  // true solo nella prima invocazione generale fatta nella funzione public void mineRec(float[] prefix, List<Item> extensions, float minSup, String representation ) 
          if (tidLists) {
        	// store the tids of tid1 not present in tid2
        	// Modify the function comparing only the even elements(tids, odd terms are weights)
        	// condTids contiene gli i tids che sono in tids1 ma non in tids2
                condTids = setDifference(tids1, tids2);
          } else {
        	// store the tids of tid2 not present in tid1
        	// In questo caso fa il contrario del ramo vero
            condTids = setDifference(tids2, tids1);
          }
          
	  // Calcola il supporto della differenza (ossia dei tidset presenti solo in condTids) come la somma delle transazioni presenti in condTids
          // Modify the method to evaluate the weight
          float supDiff = 0;
          // iterate on the matrix of the tids
          for (float[] partTids : condTids.tids) {
             if (partTids != null) {
            	int j = 0;
              	for(float num : partTids){
               		if((j%2) == 1){
               			supDiff += num;
               		}
               		j++;
               	}
              }
          }

          float newSupport = 0;
          // must evaluate if there aren't elements in common between the itemsets (exclude immediately the new itemset) 
          // Se i tids che sono nella differenza sono uguali a quello dell'item1 allora vuol dire che non ci sono transazioni
          // in comune tra item1 e l'item2 che sto considerando come possibile estensione. Quindi quell'estensione non sara' mai frequente
          // e posso non considerarlo. Per non considerarlo impone il su supporto a minSup -1 in modo da renderlo infrequente (non mi 
          // interessa il valore esatto del supporto. Mi serve solo sapere che e' infrequente e basta)
          if(condTids.size() == tids1.size() && tidLists == true){
        	  // if don't have tids in common, allocate newSupport a to any value <  minSup 
        	  newSupport = minSup-1;
          } else {
		  // Il supporto dell'itemset dato da newPrefix + item2 e' pari a quello di newPrefix meno quello delle transazioni
		  // presenti nella tidlist di item1 ma non in quella di item2	
        	  // otherwise calculate the newSupport
        	  newSupport = support - supDiff;
          }
          
          if (newSupport >= minSup) {
	    // Itemset ottenuto da prefix + item2 e' frequente e quindi aggiungo item2 tra quelli da usare per l'estensione	
	    // con l'informazione sul suo supporto e gli associa come tidlist quella della differenza tra la sua tidlist e quella dell'altro che usa per l'estesione 
            Item newItem = new Item(item2.id, newSupport, condTids);
            newItems.add(newItem);
          }
        }

	// Se ho trovato almeno un item che permette di estendere newPrefix e ottenere degli itemset ancora frequenti 
	// allora invoco declatRec passando il prefisso e gli item da usare per l'estensione (attenzione che gi item hanno sia 
	// l'id dell'item che il suo supporto nel "db proiettato" rispetto a newPrefix). Il flag delle tidlist in questo caso e' false
	// probabilmente per indicare che non passo le tidlist dei vari item, bensi' direttamente i loro supporti nel db proiettato 
        if (newItems.size() > 0) {
          // Add parameter representation
          declatRec(newPrefix, newItems, minSup, false, representation);
        }
      } else { // Non ci sono altri items con cui estedere l'itemset attuale. Se l'itemset attuale e' frequente lo salvo 
	       // nell'insieme di risultati. L'itemset attuale si trova in newPrefix
        // report if the itemset cannot be extended anymore (add parameter representation)
          // Alla fine questo viene sempre eseguito visto che c'è la stessa cosa in if(canBeExtended) che c'e' dopo.
    	  if(support >= minSup){
    		  reporter.report(newPrefix, support, representation);
    	  }
      }

      // Perche' questo pezzo e' qui e non al fondo della parte then dell'if di prima? canBeExtended non cambia
      // Se non ho commesso errori nell'analisi posso togliere l'else dell'if prima e eseguire sempre questo blocco di codice (che 
      // e' uguale a quello dell'else dell'if prima) 	
      if(canBeExtended){
        // do not report closed itemsets
    	// control the support
    	  if(support >= minSup){
    		  reporter.report(newPrefix, support, representation);
    	  }
      }
    
      // Non capisco perche' deve terminare il ciclo for se newPrefix.length < maxSize e  closureCheck(item1)
      // Se newPrefix.length < maxSize ha senso fare l'espansione
      // closeCheck() restituisce true se item1 ha una tidlist vuota (ossia ha supporto 0?). Questo dovrebbe voler dire 
      // che ha una tidlist che e' uguale a quella degli items usati per estendere l'itemset attuale e quindi lui non e' parte di un closed
      // visto che c'è qualcosa che lo estende che ha il suo stesso supporto (e stessa tidlist).
      // Commento questo pezzo per fare in modo che estragga tutti gli itemset e non solo i (pesudo) closed
      /*if (newPrefix.length < maxSize && closureCheck(item1)) {
        break;
      } */
    }
  }
  

private static boolean closureCheck(Item item) {
    return item.getTids().size() == 0;
  }


}
