/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.eclat.util;

import static ua.ac.be.fpm.eclat.util.TrieDumper.CLOSESUP;
import static ua.ac.be.fpm.eclat.util.TrieDumper.OPENSUP;
import static ua.ac.be.fpm.eclat.util.TrieDumper.SEPARATOR;
import static ua.ac.be.fpm.eclat.util.TrieDumper.SYMBOL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper.Context;

/**
 * Implementation of a Set Reporter that writes multiple frequent itemsets with their corresponding supports at once in
 * a compressed Trie String representation.
 *
 * The reporter will use the last reported itemset for building the Trie String. As such, reporting new itemsets in
 * depth-first manner, prefixes can be combined more often which results in better compression.
 */
@SuppressWarnings({"unchecked", "rawtypes"})
//SetReporter is only an interface in which methods are defined (empty)
public class TreeStringReporter implements SetReporter {
  private static final int MAX_SETS_BUFFER = 1000000;

  private final Context context;
  private final StringBuilder builder;

  private float[] prevSet;
  private int count;
  // Add a list for the representation single itemset (new created class)
  private List<Itemset> itemsets = new ArrayList<Itemset>();

  public TreeStringReporter(Context context) {
    this.context = context;
    builder = new StringBuilder();
    count = 0;
  }

  @Override
  public void report(float[] itemset, float support, String representation) {
	// Add the control of representation option
	if(representation.equals("compact")){
		// Add a variable to understand if depth is > of itemset.lenght-1 (before error on print)
		boolean aggiungi = true;

	    if (prevSet == null) {
	      for (int i = 0; i < itemset.length - 1; i++) {
	        // SEPARATOR = |
	        builder.append(itemset[i]).append(SEPARATOR);
	      }
	    } else {
	      int depth = 0;
          // count how many items are equal between itemset and prevSet
	      while (depth < itemset.length && depth < prevSet.length && itemset[depth] == prevSet[depth]) {
	        depth++;
	      }
	      
          // insert #symbol $ for any items that differ from prevSet
	      for (int i = prevSet.length - depth; i > 0; i--) {
	        builder.append(SYMBOL);
	      }
	      for (int i = depth; i < itemset.length - 1; i++) {
	    	// copy the items that differ from the previous without copying the last item of the itemset
	        builder.append(itemset[i]).append(SEPARATOR);
	      }
	      // CHANGE: need to verify if not do previous for because
	      // i  that is depth = itemset.lenght-1 or depth > itemset.lenght-1
	      if(depth > itemset.length -1){
	    	  aggiungi = false;
	      }
	    }
	    // CHANGE: must write the last element of itemset only if aggiungi=true
	    if(aggiungi){
	    	// copy the last item of itemset that i have not copied in the previous for
	    	builder.append(itemset[itemset.length - 1]).append(OPENSUP).append(support).append(CLOSESUP);
	    } else {
	    	// don't copy the last item because is equal at the item of the previous set
	    	builder.append(OPENSUP).append(support).append(CLOSESUP);
	    }
	    
	    prevSet = Arrays.copyOf(itemset, itemset.length);
	} else {
		// Add print not compact(single itemset)
		StringBuilder str = new StringBuilder();
		int i;
		// print all the itemset (not only the difference with the previous with the same prefix)
		for(i = 0; i < itemset.length -1; i++){
			str.append(itemset[i]).append(" ");
		}
		str.append(itemset[i]);
		Itemset itm = new Itemset(str.toString(), support);
	
		itemsets.add(itm);


		// Stampo anche il precedente 
		/*if (prevSet != null) {
			// Add print not compact(single itemset)
			str = new StringBuilder();
			// print all the itemset (not only the difference with the previous with the same prefix)
			str.append("10000 ");
			for(i = 0; i < prevSet.length -1; i++){
				str.append(prevSet[i]).append(" ");
			}
			str.append(prevSet[i]);
			itm = new Itemset(str.toString(), support);
		
			itemsets.add(itm);
		}

	        prevSet = Arrays.copyOf(itemset, itemset.length);
                */
		// Fine gestione itemset precedente

	}

    count++;
    if (count % MAX_SETS_BUFFER == 0) {
      try {
        context.write(new Text("" + count), new Text(builder.toString()));
      } catch (IOException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println("wrote " + count + " compressed itemsets");
      // empty the builder
      builder.setLength(0);
      count = 0;
    }
  }

  @Override
  // Add the choice of the representation
  public void close(String representation) {
    try {
      if(count != 0){
    	  // Add the control on the representation
    	  if(representation.equals("compact")){
    		  context.write(new FloatWritable(count), new Text(builder.toString()));
              System.out.println("wrote " + count + " compressed itemsets");
    	  } else {
    		  for(Itemset itms : itemsets){
    			  context.write(new FloatWritable(itms.support), new Text(itms.items));
    		  }
    		  System.out.println("wrote " + count + " single itemsets");
    	  }
          builder.setLength(0);
          count = 0;  
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }



  // Add to avoid errors given that in SetReporter(interface) is declared also this method
  @Override
  public void close() {
	// TODO Auto-generated method stub
	
  }
  
  //Add to avoid errors given that in SetReporter(interface) is declared also this method
  @Override
  public void report(float[] itemset, float support) {
	// TODO Auto-generated method stub
	
 }
}
