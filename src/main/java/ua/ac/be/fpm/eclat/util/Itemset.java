package ua.ac.be.fpm.eclat.util;

// CLASSE AGGIUNTA x la stampa single itemset
public class Itemset {
	public final String items;
	public final float support;
	
	public Itemset(String items, float support){
		this.items = items;
		this.support = support;
	}
	
	@Override
	public String toString() {
	    return items + " (" + support + ")";
	}

}
