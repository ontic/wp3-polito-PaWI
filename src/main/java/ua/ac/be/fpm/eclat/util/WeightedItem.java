package ua.ac.be.fpm.eclat.util;

// AGGIUNTA nuova classe
public class WeightedItem {
	
	public final int itemId;
	public float weight;
	
	public WeightedItem(int itemId, float weight){
		this.itemId = itemId;
		this.weight = weight;
	}
	
	@Override
	public String toString() {
	    return itemId + " (" + weight + ")";
	}

}
