/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.eclat;

import static ua.ac.be.fpm.util.FIMOptions.REPRESENTATION_KEY;

import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

/**
 * Reducer class for Eclat phase of BigFIM and DistEclat. It cumulates all itemsets reported as compressed tree strings
 * and writes them to file.
 * 
 * <pre>
 * {@code
 * Original Input Per Mapper:
 * 
 * 1 2                                      | Mapper 1
 * 1                                        | Mapper 1
 * 
 * 1 2 3                                    | Mapper 2
 * 1 2                                      | Mapper 2
 * 
 * 1 2                                      | Mapper 3
 * 2 3                                      | Mapper 3
 * 
 * 
 * 
 * Example MinSup=1, K=2:
 * ======================
 * 
 * Input:
 * Text                   Iterable<Text>
 * (Empty)                (<Frequent itemsets in trie format>)
 * "1"                    <"2|3(2)">
 * "3"                    <"1|3(1)2(1)$$2(4)">
 * 
 * Output                 Text
 * (Empty)                (Frequent itemsets in trie format)
 * "1"                    <"2|3(2)">
 * "3"                    <"1|3(1)2(1)$$2(4)">
 * }
 * </pre>
 */
public class EclatMinerReducer extends Reducer<FloatWritable,Text,FloatWritable,Text> {
  
  private long setsFound = 0;
  //AGGIUNTA
  private String representation;
  
  
  // AGGIUNTA
  @Override
  public void setup(Context context) {
    Configuration conf = context.getConfiguration();
    representation = conf.get(REPRESENTATION_KEY, "single");    
  }
  
  
  @Override
  public void reduce(FloatWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
	// MODIFICA il num di sets cambia in base alla rappresentazione
	if(representation.equals("compact")){
		long numberOfSets = Long.parseLong(key.toString());
		for (Text itemsets : values) {
			setsFound += numberOfSets;
		    context.write(key, itemsets);
		}
	} else {
		for (Text itemsets : values) {
			setsFound++;
			// AGGIUNTA WEIGHT_4 ordino gli item nell'itemset
			String set = itemsets.toString();
			String[] vet = set.split(" ");
			int length = vet.length;
			int[] arr = new int[length];
			int i = 0;

			for(String it : vet){
				// item
				int num = Integer.parseInt(it.replaceAll("\\.0",""));
				arr[i] = num;

				i++;
			}
			// Sort items
			Arrays.sort(arr);
			StringBuilder str = new StringBuilder();
			int j;
			for(j = 0; j < arr.length-1; j++){
				str.append(arr[j]).append(" ");
			}
			str.append(arr[j]);

			String itset = str.toString();
			context.write(key, new Text(itset));
		}
	}
  }
  
  @Override
  public void cleanup(Context context) {
    System.out.println("Mined " + setsFound + " itemsets");
  }
}
