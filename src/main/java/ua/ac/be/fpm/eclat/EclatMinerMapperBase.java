/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.eclat;

import static com.google.common.collect.Lists.newArrayList;
import static ua.ac.be.fpm.util.FIMOptions.MIN_SUP_KEY;
import static ua.ac.be.fpm.util.FIMOptions.OUTPUT_DIR_KEY;
import static ua.ac.be.fpm.util.FIMOptions.PREFIX_LENGTH_KEY;
import static ua.ac.be.fpm.util.FIMOptions.REPRESENTATION_KEY;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;

import ua.ac.be.fpm.eclat.util.Item;
import ua.ac.be.fpm.eclat.util.SetReporter;
import ua.ac.be.fpm.hadoop.util.FloatArrayWritable;
import ua.ac.be.fpm.hadoop.util.FloatMatrixWritable;

/**
 * MapperBase class for the Eclat phase of BigFIM and DistEclat. This mapper mines the frequent itemsets for the
 * specified prefixes (subtree).
 */
public abstract class EclatMinerMapperBase<VALUEOUT> extends Mapper<FloatArrayWritable,FloatMatrixWritable,FloatWritable,VALUEOUT> {

  private float minSup;

  protected abstract SetReporter getReporter(Context context);

  float[] prefix;
  List<Item> extensions;

 private int mapperId = 0;
 private int num;
 // Add a variable to choice the representation
 private String representation;
 private String dir;


  public EclatMinerMapperBase() {
    super();
  }

  @Override
  public void setup(Context context) throws IOException {
    Configuration conf = context.getConfiguration();
    
    // take the output directory
    dir = conf.get(OUTPUT_DIR_KEY, "");
    // take minSup from conf (set from command line), otherwise default value 1
    minSup = conf.getFloat(MIN_SUP_KEY, 1);

    extensions = newArrayList();
    
    // take the number of the mapper
    mapperId = context.getTaskAttemptID().getTaskID().getId();
    
    // take the type of representation (single default)
    representation = conf.get(REPRESENTATION_KEY, "single");
   
	num = 0;
  }

  @Override
  public void map(FloatArrayWritable key, FloatMatrixWritable value, Context context) throws IOException,
      InterruptedException {
    Writable[] keyWritables = key.get();
    Writable[] valueWritables = value.get();

    if (keyWritables.length == 0 && valueWritables.length == 0) {
      // this is the last row    	
      mineSubTree(context);
      prefix = null;
      extensions.clear();
    } else if (valueWritables.length == 0) {
      // this is the 1st row (the key is the prefix)
      prefix = new float[keyWritables.length];
      int i = 0;
      for (Writable w : keyWritables) {
        prefix[i++] = ((FloatWritable) w).get();
      }
    } else {
   	  // these are the intermediate rows (keys are items, values are tidLists)
      int item = (int)((FloatWritable) keyWritables[0]).get();
      float[][] tids = value.toFloatMatrix();

      // Change the mode to calculate the support
      float support = 0;
      for (float[] partTids : tids) {
        if (partTids != null) {
        	int j = 0;
        	// take only the odd terms (the weight)
        	for(float number : partTids){
        		if((j%2) == 1){
            		support += number;
        		}
        		j++;
        	}
        }
      }
      extensions.add(new Item(item, support, tids));
    }
  }



@Override
  public void cleanup(Context context) throws IOException, InterruptedException {
    mineSubTree(context);
    // take the mapperId
    int mapperId = context.getTaskAttemptID().getTaskID().getId();
	if(mapperId == 0){
		  // print the results of the Apriori phase only in the 1st mapper
		  stampaApriori(context);
	}
  }

  private void mineSubTree(Context context) throws IOException, InterruptedException {
    if (prefix == null || prefix.length == 0) {
      return;
    }
    
    // sort extensions in ascending order
    Collections.sort(extensions, new EclatMiner.AscendingItemComparator());

    StringBuilder builder = new StringBuilder();
    builder.append("Run eclat: prefix: ");
    for (float p : prefix) {
      builder.append(p + " ");
    }
    builder.append("#items: " + extensions.size());
    System.out.println(builder.toString());
    EclatMiner miner = new EclatMiner();
    // call the implementation of EclatMinerMapper, where create a new variable TreeStringReporter(modified) 
    SetReporter reporter = getReporter(context);
    // assign the parameter at variable reporter of the miner
    miner.setSetReporter(reporter);
    // mine of the tree associated to the prefix(method defined in class EclatMiner)
    // Add the parameter representation
    miner.mineRec(prefix, extensions, minSup, representation);
    // close defined in TreeStringReporter, add representation
    reporter.close(representation);
  }
  
  
  /*
   * Print the results of Apriori phase in the context
   */
  public static void stampaApriori(org.apache.hadoop.mapreduce.Mapper.Context context) throws IOException, InterruptedException {
	    Configuration conf = context.getConfiguration();
	    String dir = conf.get(OUTPUT_DIR_KEY, "");
	    // take the directory where results of Apriori are saved
	    String aprioriDir = dir + "/ap";
		String delimiter = "\t";
	    
	    // take the prefix
	    int prefix = conf.getInt(PREFIX_LENGTH_KEY, 1);
	    int i = 1;
	    // open the results of all Apriori phases
	    while(i <= prefix){
	    	Path pt=new Path(aprioriDir + i + "/part-r-00000");
	    	FileSystem fs = FileSystem.get(new Configuration());
			// open the file in read mode
		    BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(pt)));
		    // iterate on each row of the file
		    String line = reader.readLine();
		    while(line != null){
		    	String[] itemsSplit = line.split(delimiter);
		    	Float w = Float.parseFloat(itemsSplit[1]);
		    	// copy the results in context
		    	context.write(new FloatWritable(w), new Text(itemsSplit[0]));
		    	line = reader.readLine();
		    }
		    i++;
	    }
	
}

}
