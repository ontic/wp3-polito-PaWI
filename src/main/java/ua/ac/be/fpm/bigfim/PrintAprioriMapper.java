package ua.ac.be.fpm.bigfim;

import static ua.ac.be.fpm.util.FIMOptions.PREFIX_LENGTH_KEY;
// ADD
import static ua.ac.be.fpm.util.FIMOptions.OUTPUT_DIR_KEY;
import static java.io.File.separator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.ContentSummary;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ua.ac.be.fpm.hadoop.util.FloatArrayWritable;
import ua.ac.be.fpm.hadoop.util.FloatMatrixWritable;

public class PrintAprioriMapper extends Mapper<LongWritable,Text,FloatWritable,Text> {
	
	
	 public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		 
	 }
	 
	 
	 public void cleanup(Context context) throws IOException, InterruptedException {
		// take the mapperId
		int mapperId = context.getTaskAttemptID().getTaskID().getId();
		if(mapperId == 0){
			// print the results of the Apriori phase only in the 1st mapper
			stampaApriori(context);
		}
	  }

	 
	 /*
	   * Print the results of Apriori phase in the context
	  */
	  public static void stampaApriori(org.apache.hadoop.mapreduce.Mapper.Context context) throws IOException, InterruptedException {
		    Configuration conf = context.getConfiguration();
		    String dir = conf.get(OUTPUT_DIR_KEY, "");
		    // take the directory where results of Apriori are saved
		    String aprioriDir = dir + separator + "ap";
			String delimiter = "\t";
		      
		 
		    // take the prefix
		    int prefix = conf.getInt(PREFIX_LENGTH_KEY, 1);
		    int i = 1;
		    // open the results of all Apriori phases
		    while(i <= prefix){
		    	String str = aprioriDir + i + "/part-r-00000";
		    	// 
		    	Path path = new Path(str);
		        FileSystem hdfs = path.getFileSystem(conf);
		        ContentSummary cSummary = hdfs.getContentSummary(path);
		        long size = cSummary.getLength();
		        if(size == 0){
		      	  break;
		        }
		        Path pt=new Path(str);
		    	FileSystem fs = FileSystem.get(new Configuration());
		    	// open the file in read mode
			    BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(pt)));
			    // iterate on each row of the file
			    String line = reader.readLine();
			    while(line != null){
			    	String[] itemsSplit = line.split(delimiter);
			    	Float w = Float.parseFloat(itemsSplit[1]);
			    	// copy the results in context
			    	context.write(new FloatWritable(w), new Text(itemsSplit[0]));
			    	line = reader.readLine();
			    }
			    i++;
		    }
		
	}
}
