/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.bigfim;

import static com.google.common.collect.Maps.newHashMap;
import static org.apache.hadoop.filecache.DistributedCache.getLocalCacheFiles;
import static ua.ac.be.fpm.bigfim.Tools.convertLineToSet;
import static ua.ac.be.fpm.bigfim.Tools.createCandidates;
import static ua.ac.be.fpm.bigfim.Tools.getSingletonsFromSets;
import static ua.ac.be.fpm.bigfim.Tools.readItemsetsFromFile;
import static ua.ac.be.fpm.util.FIMOptions.DELIMITER_KEY;
// AGGIUNTA
import static ua.ac.be.fpm.bigfim.PreProcessing.preProcess;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Mapper class for Apriori phase of BigFIM. Each mapper receives a sub part (horizontal cut) of the dataset and
 * combines a list of base itemsets in candidates of length+1 for its sub database. The latter are counted in the map
 * function. If no base itemsets are specified, all singletons are counted. The size of the sub part is depending on the
 * number of mappers and the size of the original dataset.
 * 
 * <pre>
 * {@code
 * Original Input Per Mapper:
 * 
 * 1 2                                      | Mapper 1
 * 1                                        | Mapper 1
 * 
 * 1 2 3                                    | Mapper 2
 * 1 2                                      | Mapper 2
 * 
 * 1 2                                      | Mapper 3
 * 2 3                                      | Mapper 3
 * 
 * 
 * 
 * Example Phase=1, MinSup=1:
 * ==========================
 * 
 * Input:
 * LongWritable   Text
 * (Offset)       (Transaction)
 * 0              "1 2"                     | Mapper 1
 * 4              "1"                       | Mapper 1
 * 
 * 6              "1 2 3"                   | Mapper 2
 * 12             "1 2"                     | Mapper 2
 * 
 * 16             "1 2"                     | Mapper 3
 * 20             "2 3"                     | Mapper 3
 * 
 * Output:
 * Text           IntWritable
 * (Itemset)      (Support)
 * "1"            2                         | Mapper 1
 * "2"            1                         | Mapper 1
 * 
 * "1"            2                         | Mapper 2
 * "2"            2                         | Mapper 2
 * "3"            1                         | Mapper 2
 * 
 * "1"            1                         | Mapper 3
 * "2"            2                         | Mapper 3
 * "3"            1                         | Mapper 3
 * 
 * 
 * 
 * Example Phase=2, MinSup=1:
 * ==========================
 * 
 * Itemsets:
 * 1
 * 2
 * 3
 * 
 * Itemsets of length+1:
 * 1 2
 * 1 3
 * 2 3
 * 
 * Input:
 * LongWritable   Text
 * (Offset)       (Transaction)
 * 0              "1 2"             | Mapper 1
 * 4              "1"               | Mapper 1
 * 
 * 6              "1 2 3"           | Mapper 2
 * 12             "1 2"             | Mapper 2
 * 
 * 16             "1 2"             | Mapper 3
 * 20             "2 3"             | Mapper 3
 * 
 * Output:
 * Text           IntWritable
 * (Itemset)      (Support)
 * "1 2"          1                 | Mapper 1
 * 
 * "1 2"          2                 | Mapper 2
 * "1 3"          1                 | Mapper 2
 * "2 3"          1                 | Mapper 2
 * 
 * "1 2"          1                 | Mapper 3
 * "2 3"          1                 | Mapper 3
 * }
 * </pre>
 */
// Modified mapperOutputValue
public class AprioriPhaseMapper extends Mapper<LongWritable,Text,Text,FloatWritable> {
  
  static class Trie {
    final int id;
    float support;			//Modified: from int to float
    boolean leafnode;		//Modified: add this variable to decide which nodes print
    						//	(the support can be = 0 caused by sum of negative terms)
    final Map<Integer,Trie> children;
    
    public Trie(int id) {
      this.id = id;
      this.support = 0;
      this.leafnode = false;
      children = newHashMap();
    }
    
    // Add the weight and the variable leafnode (need to distinguish which nodes print)
    public void incrementSupport(Float weight) {
      leafnode = true;
      support += weight;
    }
    
    // return child with parameter id if exist, otherwise create it 
    public Trie getChild(int id) {
      Trie child = children.get(id);
      if (child == null) {
        child = new Trie(id);
        children.put(id, child);
      }
      return child;
    }
    
    @Override
    public String toString() {
      return "[" + id + "(" + support + "):" + children + "]";
    }
    
  }
  
  private Set<Integer> singletons;
  private Trie countTrie;
  private List<Float> weightList = new ArrayList<Float>();			// variable added
  private int phase = 1;
  private String delimiter1;		// variable added
  private String delimiter2;		// variable added
  int mapperId;						// variable added
  
  @Override
  public void setup(Context context) throws IOException {
    Configuration conf = context.getConfiguration();
    // return the value of delimeter, if is null return the 2nd parameter (defaultValue)
    delimiter2 = conf.get(DELIMITER_KEY, " ");
    delimiter1 = "\t";
    
    // return the value of mapred.cache.localFiles in conf
    Path[] localCacheFiles = getLocalCacheFiles(conf);
    countTrie = new Trie(-1);
    // if is the 1st mapper execution is null (no files in cache with 1st Apriori phase run)
    if (localCacheFiles != null) {
    	
      // take the cache file (result of previous Apriori phase) 
      String filename = localCacheFiles[0].toString();
      
      // return the list of itemsets saved in the file (take only items not the support)
      List<SortedSet<Integer>> itemsets = readItemsetsFromFile(filename);
      
      // create the list of candidates (itemsets with lenght "phase", combining the
      // itemset of previous stage (phase-1) )
      Collection<SortedSet<Integer>> candidates = createCandidates(itemsets);
     
      // take the items from candidates (don't copy the duplicate items)
      singletons = getSingletonsFromSets(candidates);
      
      // take the length of 1st itemsets (all elements have same length)
      phase = itemsets.get(0).size() + 1;
      
      // initialize the tree with candidates
      countTrie = initializeCountTrie(candidates);
      
      System.out.println("Singletons: " + singletons.size());
      System.out.println("Words: " + candidates.size());
      
    }
  }
  

  @Override
  public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    // Add preProcess
	// The input row is of the type:
	// offset		item:weight item:weight
	List<String> rows = preProcess(value);  
    
	// Change how to manage the input (before one row, now a list of rows)
	for(String line : rows){
		// row of the type:
		// weight		item1 item2 ..... item27
		String[] itemsSplit = line.split(delimiter1);
		// take the weight
		Float weight = Float.valueOf(itemsSplit[0]);
		weightList.add(weight);
		
	    // the 1st time that execute the Mapper: phase = 1 (true) & singletons empty
	    // convert the line in input at map in a list, if is the 1st execution of the mapper store all the items
	    // if not control that the item is in the singleton's list and after store the items
		List<Integer> items = convertLineToSet(itemsSplit[1], phase == 1, singletons, delimiter2);
		incrementSubSets(items, weight);
	}
  }
  
  @Override
  // Add a variable (true) that indicate that is the 1st execution of recReport
  public void cleanup(Context context) throws IOException, InterruptedException {
	// print the elements(leaves node) of the Trie in the context 
    recReport(context, new StringBuilder(), countTrie, true);
  }
  
  /**
   * Create the Trie starting from list of candidates (without evaluate the support)
   */
  private static Trie initializeCountTrie(Collection<SortedSet<Integer>> candidates) {
    Trie countTrie = new Trie(-1);
    for (SortedSet<Integer> candidate : candidates) {
      // assign to trie always root -1
      Trie trie = countTrie;
      // iterate on the items present in a candidate
      Iterator<Integer> it = candidate.iterator();
      while (it.hasNext()) {
    	// if the item exist at this level return the element, otherwise create the leaf node
        trie = trie.getChild(it.next());
      }
    }
    // return the trie
    return countTrie;
  }
  
  /**
   * Print the tree on context
   * CHANGES:
   * - to control what to print don't see if sup!=0, but if leafnode==true
   *   ((the support could be = 0 caused by sum of negative terms)
   * - IntWritable become FloatWritable (support from int to float)
   */
  private void recReport(Context context, StringBuilder builder, Trie trie, boolean firstTime) throws IOException, InterruptedException {
	int length = builder.length();
    for (Entry<Integer,Trie> entry : trie.children.entrySet()) {
      Trie recTrie = entry.getValue();
      builder.append(recTrie.id + " ");
      // leaf node print the builder(itemset), if the support != 0
      if (recTrie.children.isEmpty()) {
    	  // CHANGE: to control what to print don't see if sup!=0, but if leafnode==true 
        if (recTrie.leafnode == true) {
          // take the substring excluding the last character(final space)
          Text key = new Text(builder.substring(0, builder.length() - 1));
          // CHANGE: FloatWritable no more IntWritable
          FloatWritable value = new FloatWritable(recTrie.support);
          // write itemset and support
          context.write(key, value);
        }
      } else {
    	// recur on Trie to search leafnode
        recReport(context, builder, recTrie, false);
      }
      
      builder.setLength(length);
    }
  }
  
  /**
   * Increment the support
   * CHANGES:
   * - add the parameter weight in input and modify incrementSupport (before increment of 1, now of support)
   */
  private void incrementSubSets(List<Integer> items, Float weight) {
	// if the #items is less than phase is useless do anything (too short itemset)
    if (items.size() < phase) {
      return;
    }
    
    // in the 1st phase the tree have only one level (excluding the root)
    if (phase == 1) {
      for (int i = 0; i < items.size(); i++) {
    	// if the item is present in the Trie (at 1st level, son of -1, countTrie) return the element
      	// if not add at the Trie
        Trie recTrie = countTrie.getChild(items.get(i));
     // CHANGE: now increase the support of weight (before of 1)
        recTrie.incrementSupport(weight);
      }
      return;
    }
    
    // do recursive passage if phase != 1, i need to explore the Trie(multiple level)
    doRecursiveCount(items, 0, countTrie, weight);
  }
  
  /**
   * Increment the support if phase > 1
   * CHANGES:
   * - pass also the parameter weight that use in incrementSupport
   */
  private void doRecursiveCount(List<Integer> items, int ix, Trie trie, Float weight) {
    for (int i = ix; i < items.size(); i++) {
      // return the element if exist at this level, otherwise null 
      Trie recTrie = trie.children.get(items.get(i));
      if (recTrie != null) {
        if (recTrie.children.isEmpty()) {
          // CHANGE: increase the support of leaf node of weight (before 1)
          recTrie.incrementSupport(weight);
        } else {
          // recur if the element has children
          doRecursiveCount(items, i + 1, recTrie, weight);
        }
      }
    }
  }
}