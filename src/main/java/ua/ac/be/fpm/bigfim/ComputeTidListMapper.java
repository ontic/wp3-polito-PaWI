/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.bigfim;

import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Maps.newHashMap;
import static org.apache.hadoop.filecache.DistributedCache.getLocalCacheFiles;
import static ua.ac.be.fpm.bigfim.PreProcessing.preProcess;
import static ua.ac.be.fpm.bigfim.Tools.convertLineToSet;
import static ua.ac.be.fpm.bigfim.Tools.createCandidates;
import static ua.ac.be.fpm.bigfim.Tools.readItemsetsFromFile;
import static ua.ac.be.fpm.util.FIMOptions.DELIMITER_KEY;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import ua.ac.be.fpm.hadoop.util.FloatArrayWritable;

/**
 * Mapper class for the second phase of BigFIM. Each mapper receives a sub part (horizontal cut) of the dataset and
 * computes partial tidlists of the given itemsets in the sub database. The size of the sub database depends on the
 * number of mappers and the size of the original dataset.
 * 
 * <pre>
 * {@code
 *  Original Input Per Mapper:
 * 
 * 1 2                                      | Mapper 1
 * 1                                        | Mapper 1
 * 
 * 1 2 3                                    | Mapper 2
 * 1 2                                      | Mapper 2
 * 
 * 1 2                                      | Mapper 3
 * 2 3                                      | Mapper 3
 * 
 * 
 * 
 * Example Phase=1, MinSup=1:
 * ==========================
 * 
 * Input:
 * LongWritable   Text
 * (Offset)       (Transaction)
 * 0              "1 2"                     | Mapper 1
 * 4              "1"                       | Mapper 1
 * 
 * 6              "1 2 3"                   | Mapper 2
 * 12             "1 2"                     | Mapper 2
 * 
 * 16             "1 2"                     | Mapper 3
 * 20             "2 3"                     | Mapper 3
 * 
 * Output:
 * Text           IntArrayWritable
 * (Prefix)       ([Mapper ID, Item, Partial Tids...])
 * ""             [1,1,0,1]                 | Mapper 1
 * ""             [1,2,0]                   | Mapper 1
 * 
 * ""             [2,1,0,1]                 | Mapper 2
 * ""             [2,2,0,1]                 | Mapper 2
 * ""             [2,3,0]                   | Mapper 2
 * 
 * ""             [3,1,0]                   | Mapper 3
 * ""             [3,2,0,1]                 | Mapper 3
 * ""             [3,3,1]                   | Mapper 3
 * 
 * 
 * 
 * Example Phase=2, MinSup=1:
 * ==========================
 * 
 * Itemsets:
 * 1
 * 2
 * 3
 * 
 * Itemsets of length+1:
 * 1 2
 * 1 3
 * 2 3
 * 
 * Input:
 * LongWritable   Text
 * (Offset)       (Transaction)
 * 0              "1 2"                     | Mapper 1
 * 4              "1"                       | Mapper 1
 * 
 * 6              "1 2 3"                   | Mapper 2
 * 12             "1 2"                     | Mapper 2
 * 
 * 16             "1 2"                     | Mapper 3
 * 20             "2 3"                     | Mapper 3
 * 
 * Output:
 * Text           IntArrayWritable
 * (Prefix)       ([Mapper ID, Item, Partial Tids...])
 * "1"            [1,2,0]                   | Mapper 1
 * 
 * "1"            [2,2,0,1]                 | Mapper 2
 * "1"            [2,3,0]                   | Mapper 2
 * "2"            [2,3,0]                   | Mapper 2
 * 
 * "1"            [3,2,0]                   | Mapper 3
 * "2"            [3,3,1]                   | Mapper 3
 * }
 * </pre>
 */
public class ComputeTidListMapper extends Mapper<LongWritable,Text,Text,FloatArrayWritable> {
  
  private static int TIDS_BUFFER_SIZE = 100000000;
  
  // structure that identify a node (item)
  static class Trie {
    public int id;
    public List<Float> tids;
    public Map<Integer,Trie> children;
    
    public Trie(int id) {
      this.id = id;
      tids = newLinkedList();
      children = newHashMap();
    }
    
    // return child with parameter id if exist, otherwise create it 
    public Trie getChild(int id) {
      Trie child = children.get(id);
      if (child == null) {
        child = new Trie(id);
        children.put(id, child);
      }
      return child;
    }
    
    // add tid at the tidList and also the weight (CHANGE)
    public void addTid(int counter, float weight) {
      tids.add((float)counter);
      tids.add(weight);
    }
    
  }
  
  // Change the type from IntArrayWritable to FloatArrayWritable
  private final FloatArrayWritable iaw;
  
  private Set<Integer> singletons;
  private Trie countTrie;
  
  private int phase = 1;
  
  private int counter;
  private int tidCounter = 0;
  private int id;
   
  private String delimiter1;
  private String delimiter2;
  
  public ComputeTidListMapper() {
    iaw = new FloatArrayWritable();
    singletons = null;
    countTrie = new Trie(-1);
    counter = 0;
    id = -1;
    phase = 1;
  }
  
  @Override
  public void setup(Context context) throws IOException {
    Configuration conf = context.getConfiguration();
    // return the value of delimeter, if is null return the 2nd parameter (defaultValue)
    delimiter2 = conf.get(DELIMITER_KEY, " ");
    delimiter1 = "\t";
    
    // return the value of mapred.cache.localFiles in conf
    Path[] localCacheFiles = getLocalCacheFiles(conf);
    
    if (localCacheFiles != null) {
      
      // take the cache file (the result of last Apriori phase)
      String filename = localCacheFiles[0].toString();
      
      // return the list of itemsets saved in the file (take only items not the support)
      List<SortedSet<Integer>> itemsets = readItemsetsFromFile(filename);
      
      // create the list of candidates (itemsets with lenght "phase", combining the
      // itemset of previous stage (phase-1) )
      Collection<SortedSet<Integer>> candidates = createCandidates(itemsets);
   
      // take the items from candidates (don't copy the duplicate items)
      singletons = Tools.getSingletonsFromSets(candidates);
      
      // initialize the tree with candidates
      countTrie = initializeCountTrie(candidates);
      
      // take the length of 1st itemsets (all elements have same length)
      phase = itemsets.get(0).size() + 1;
    }
    
    // represent the id of the mapper
    id = context.getTaskAttemptID().getTaskID().getId();
  }
  
  @Override
  public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
     	// Add preProcess
		// The input row is of the type:
		// offset		item:weight item:weight
		List<String> rows = preProcess(value);  
	    
		// Change how to manage the input (before one row, now a list of rows)
		for(String line : rows){
			// row of the type:
			// weight		item1 item2 ..... item27
			String[] itemsSplit = line.split(delimiter1);
			// take the weight
			Float weight = Float.valueOf(itemsSplit[0]);
			
			// the 1st time that execute the Mapper: phase = 1 (true) & singletons empty
		    // convert the line in input at map in a list, if is the 1st execution of the mapper store all the items
		    // if not control that the item is in the singleton's list and after store the items
			List<Integer> items = convertLineToSet(itemsSplit[1], phase == 1, singletons, delimiter2);
			
			// Add the tid at the tidList and also the weight
			reportItemTids(context, items, weight);
			
			// counter represent the #row(tid)
			counter++;
		}
  }
  
  @Override
  //Add a variable (true) that indicate that is the 1st execution of recReport
  public void cleanup(Context context) throws IOException, InterruptedException {
    if (tidCounter != 0) {
      // print the elements(leaves node) of the Trie in the context
      doRecursiveReport(context, new StringBuilder(), 0, countTrie, true);
    }
  }
  
  /**
   * Create the Trie starting from list of candidates (without value the support)
   */
  private static Trie initializeCountTrie(Collection<SortedSet<Integer>> candidates) {
    Trie countTrie = new Trie(-1);
    for (SortedSet<Integer> candidate : candidates) {
      // assign to trie always root -1
      Trie trie = countTrie;
      // iterate on the items present in a candidate
      for (int item : candidate) {
    	// if the item exist at this level return the element, otherwise create the leaf node
        trie = trie.getChild(item);
      }
    }
    // return the trie
    return countTrie;
  }
  
  /**
   * Create an array of length (2+tids.size) in which the 1st element is the mapper id
   * Change all the elements from Int to Float
   */
  private FloatWritable[] createFloatWritableWithIdSet(int numberOfTids) {
    FloatWritable[] iw = new FloatWritable[numberOfTids + 2];
    iw[0] = new FloatWritable(id);
    return iw;
  }
  
  /**
   * Add the tid at the tidList of the itemsets
   */
  private void reportItemTids(Context context, List<Integer> items, float weight) throws IOException, InterruptedException {
	// if the #items is less than phase is useless do anything (too short itemset)
	if (items.size() < phase) {
      return;
    }
    
	// only if no previous apriori phase
    if (phase == 1) {
      for (Integer item : items) {
    	// Add tid at item tidList and also the weight
        countTrie.getChild(item).addTid(counter, weight);
        // total number of tid
        tidCounter++;
      }
    } else {
      // recur on Trie to fill the tidList of the itemsets (Add also the weight)
      doRecursiveTidAdd(context, items, 0, countTrie, weight);
    }
    
    if (tidCounter >= TIDS_BUFFER_SIZE) {
      System.out.println("Tids buffer reached, reporting " + tidCounter + " partial tids");
      doRecursiveReport(context, new StringBuilder(), 0, countTrie, false);
      tidCounter = 0;
    }
  }
  
  /**
   *  Recur on Trie to fill the tidList of the itemsets
   */
  private void doRecursiveTidAdd(Context context, List<Integer> items, int ix, Trie trie, float weight) throws IOException,
      InterruptedException {
    for (int i = ix; i < items.size(); i++) {
      // return the element if exist at this level, otherwise null
      Trie recTrie = trie.children.get(items.get(i));
      if (recTrie != null) {
        if (recTrie.children.isEmpty()) {
          // add the tid at tidList of the element (Add also the weight)
          recTrie.addTid(counter, weight);
          tidCounter++;
        } else {
          // recur if the element has children (Add parameter weight)
          doRecursiveTidAdd(context, items, i + 1, recTrie, weight);
        }
      }
    }
  }
  
  /**
   * Print the tree on context
   */
  private void doRecursiveReport(Context context, StringBuilder builder, int depth, Trie trie, boolean firstTime) throws IOException,
      InterruptedException {
	
    int length = builder.length();
    for (Trie recTrie : trie.children.values()) {
      if (recTrie != null) {
    	// phase represent the level of leaves nodes of the Trie
        if (depth + 1 == phase) {
          if (recTrie.tids.isEmpty()) {
        	// if the leaf node have support=0 in this mapper pass at the next child
            continue;
          }
          // take the substring excluding the last character(final space)
          // in key store the prefix (path without the leaf node)
          Text key = new Text(builder.substring(0, Math.max(0, builder.length() - 1)));
          /// recTrie is the leaf node
          // create an array of length (2+tids.size) in which the 1st element is the id of the mapper (Int become Float)
          FloatWritable[] iw = createFloatWritableWithIdSet(recTrie.tids.size());
          int i1 = 1;
          // the 2nd element is the id of the leaf node
          iw[i1++] = new FloatWritable(recTrie.id);
          // next elements of the array are the tids (all become float like the support)
          for (float tid : recTrie.tids) {
            iw[i1++] = new FloatWritable(tid);
          }
          iaw.set(iw);
          // print in the context
          context.write(key, iaw);
          recTrie.tids.clear();
        } else {
          // append the items until are at the last level (leaves nodes)
          builder.append(recTrie.id + " ");
          // recur on the Trie until are at the last level
          doRecursiveReport(context, builder, depth + 1, recTrie, false);
        }
      }
      builder.setLength(length);
    }
  }
}