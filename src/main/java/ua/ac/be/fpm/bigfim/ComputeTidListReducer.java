/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.bigfim;

import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static com.google.common.collect.Maps.newHashMap;
import static ua.ac.be.fpm.hadoop.util.FloatArrayWritable.EmptyIaw;
import static ua.ac.be.fpm.hadoop.util.FloatMatrixWritable.EmptyImw;
import static ua.ac.be.fpm.util.FIMOptions.FILTER_KEY;
import static ua.ac.be.fpm.util.FIMOptions.MIN_SUP_KEY;
import static ua.ac.be.fpm.util.FIMOptions.NUMBER_OF_MAPPERS_KEY;
import static ua.ac.be.fpm.util.FIMOptions.OUTPUT_DIR_KEY;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.mutable.MutableInt;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import ua.ac.be.fpm.hadoop.util.FloatArrayWritable;
import ua.ac.be.fpm.hadoop.util.FloatMatrixWritable;

/**
 * Reducer for the second phase of BigFIM. This reducer combines partial tid lists received from different mappers for a
 * given itemset. The complete tid list is obtained by multiplying the mapper id with an offset representing the maximal
 * size of a sub database then adding the sub tid.
 * 
 * <pre>
 * {@code
 * Original Input Per Mapper:
 * 
 * 1 2                                      | Mapper 1
 * 1                                        | Mapper 1
 * 
 * 1 2 3                                    | Mapper 2
 * 1 2                                      | Mapper 2
 * 
 * 1 2                                      | Mapper 3
 * 2 3                                      | Mapper 3
 * 
 * 
 * 
 * Example Phase=1, MinSup=1:
 * ==========================
 * 
 * Input:
 * Text                   Iterable<IntArrayWritable>
 * (Prefix)               (<[Mapper ID, Item, Partial TIDs...]>)
 * ""                     <[1,1,0,1],[1,2,0],[2,1,0,1],[2,2,0,1],[2,3,0],[3,1,0],[3,2,0,1],[3,3,1]>
 * 
 * Output:
 * IntArrayWritable       IntMatrixWritable
 * (Prefix Group or Item) (Partial TIDs)
 * []                     []
 * [1]                    [[0,1],[0,1],[0]]
 * [2]                    [[0],[0,1],[0,1]]
 * [3]                    [[],[0],[1]]
 * []                     []
 * 
 * 
 * Example Phase=2, MinSup=1:
 * ==========================
 * 
 * Input:
 * Text                   Iterable<IntArrayWritable>
 * (Prefix)               (<[Mapper ID, Item, Partial TIDs...]>)
 * 1                      <[1,2,0],[2,2,0,1],[2,3,0],[3,2,0]>
 * 2                      <[2,3,0],[3,3,1]>
 * 
 * Output:
 * 
 * ==> Bucket 0
 * IntArrayWritable       IntMatrixWritable
 * (Prefix Group or Item) (Partial TIDs)
 * [1]                    []                | New prefix group
 * [2]                    [[0],[0,1],[0]]
 * [3]                    [[],[0],[]]
 * []                     []                | End of prefix group
 * 
 * ==> Bucket 1
 * IntArrayWritable       IntMatrixWritable
 * (Prefix Group or Item) (Partial TIDs)
 * [2]                    []                | New prefix group
 * [3]                    [[],[0],[1]]
 * []                     []                | End of prefix group
 * }
 * </pre>
 */
public class ComputeTidListReducer extends Reducer<Text,FloatArrayWritable,FloatArrayWritable,FloatMatrixWritable> {
  
  // max = 1 gb
  private static int MAX_FILE_SIZE = 1000000000;
  public static int MAX_NUMBER_OF_TIDS = (int) ((MAX_FILE_SIZE) * 0.7);
  
  private float minSup;
  // Add the filter
  private String filter;
  private String dir;
  
  private List<MutableInt> bucketSizes;
  
  private MultipleOutputs<FloatArrayWritable,FloatMatrixWritable> mos;
  private int numberOfMappers;
  
  
  @Override
  public void setup(Context context) throws FileNotFoundException, IOException {
    Configuration conf = context.getConfiguration();
    
    // take the output directory
    dir = conf.get(OUTPUT_DIR_KEY, "");
    // Control also the filter in the configuration
    filter = conf.get(FILTER_KEY, "yes");
    // take minSup from conf (set from command line), otherwise default value 1
    minSup = conf.getFloat(MIN_SUP_KEY, 1);
    // take numberOfMappers from conf (set from command line), otherwise default value 1
    numberOfMappers = conf.getInt(NUMBER_OF_MAPPERS_KEY, 1);
    // allocate #buckets = #mappers
    bucketSizes = newArrayListWithCapacity(numberOfMappers);
    for (int i = 0; i < numberOfMappers; i++) {
      bucketSizes.add(new MutableInt());
    }
    
    // initialize the multiple outputs and change all structure from Int to Float
    mos = new MultipleOutputs<FloatArrayWritable,FloatMatrixWritable>(context);
    
  }
  
  @Override
  // Change all the variables from Int to Float
  public void reduce(Text prefix, Iterable<FloatArrayWritable> values, Context context) throws IOException,
      InterruptedException {
    
    Map<Integer,FloatArrayWritable[]> map = newHashMap();
    for (FloatArrayWritable iaw : values) {
      Writable[] w = iaw.get();
      // 1st element of the array is the id of the mapper
      float mapperid = ((FloatWritable) w[0]).get();
      // 2nd element of the array is the id of the item
      float Item = ((FloatWritable) w[1]).get();
      int item = (int)Item;
      
      FloatArrayWritable[] tidList = map.get(item);
      // check if i have already found the item in values
      if (tidList == null) {
    	// initialize tidList (array composed by numberOfMappers lists)
        tidList = new FloatArrayWritable[numberOfMappers];
        // fill tidList with 2nd parameter
        Arrays.fill(tidList, new FloatArrayWritable(new FloatWritable[0]));
        map.put(item, tidList);
      }
      
      FloatWritable[] newTidArray;
      int mapperId = (int)mapperid;
      if (tidList[mapperId] == null) {
    	// extract the length of tidsList (the 1st two values: mapperId and itemId) => length of the array
        newTidArray = new FloatWritable[w.length - 2];
        // copy the tids of a row in one array, that after copy in the tidList at index mapperID
        for (int i = 2; i < w.length; i++) {
          newTidArray[i - 2] = (FloatWritable) w[i];
        }
      } else {
        // This is the case when the buffer size is exceeded in the mapper.
        final int oldSize = tidList[mapperId].get().length;
        int newSize = oldSize + w.length - 2;
        newTidArray = Arrays.copyOf((FloatWritable[]) tidList[mapperId].get(), newSize);
        for (int i = 0; i < w.length - 2; i++) {
          newTidArray[oldSize + i] = (FloatWritable) w[i + 2];
        }
      }
      // copy the array extracted in the tidList
      tidList[mapperId] = new FloatArrayWritable(newTidArray);
    }
    
    int totalTids = 0;
    // iterate on the map of items
    for (Iterator<FloatArrayWritable[]> it = map.values().iterator(); it.hasNext();) {
      FloatArrayWritable[] tidLists = it.next();
      float itemSupport = 0;
      // iterate on the tidList (can have 0 or 1 for each mapper)
      // Change the way to calculate the sum of tidLists
      for (FloatArrayWritable tidList : tidLists) {
    	  // take the tidList
    	  Writable[] arr = tidList.get();
  		  for(int j = 0; j < arr.length; j++){
  			 float we = 0;
  			 // take only the odd terms, that represent the weights (even terms are the tids)
  			 if((j%2) == 1){
  				String val = arr[j].toString();
  				we = Float.parseFloat(val);
  			 }
  		     itemSupport += we;
  		  }
    	  
      }
      // check if the total support of the item is > minSup
      if (itemSupport >= minSup) {
        totalTids += itemSupport;
      } else {
    	// Add the control of the filter, remove the item from the map only if filter yes
    	if(!filter.equals("no")){
    		it.remove();
    	}
      }
    }
   
    // Add the control of the filter
    if(filter.equals("no")){
    	assignToBucket(prefix, map, totalTids);
    } else {
    	if (totalTids > 0) {
    		// assign prefix at one bucket (mapper)
    		assignToBucket(prefix, map, totalTids);
    	}
    }
    
  }
  
  @Override
  public void cleanup(Context context) throws IOException, InterruptedException {
    mos.close();
  }
  
  
  /**
   * Assign prefix to one bucket (mapper) and print in the context
   */
  private void assignToBucket(Text key, Map<Integer,FloatArrayWritable[]> map, int totalTids) throws IOException,
      InterruptedException {
	// assign the lowest bucket available
    int lowestBucket = getLowestBucket();
    // check if exceeds the max number of tids
    if (!checkLowestBucket(lowestBucket, totalTids)) {
      bucketSizes.add(new MutableInt());
      lowestBucket = bucketSizes.size() - 1;
    }
    // add totalTids at the element with index lowestBucket
    bucketSizes.get(lowestBucket).add(totalTids);
    
    String baseOutputPath = "bucket-" + lowestBucket;
    
    // in the 1st row:   	prefix  []  bucket_num 
    mos.write(FloatArrayWritable.of(key.toString()), EmptyImw, baseOutputPath);
    for (Entry<Integer,FloatArrayWritable[]> entry : map.entrySet()) {
      // take the item from element of the map
      FloatArrayWritable owKey = FloatArrayWritable.of((float)entry.getKey());
      // take the tidList from element of the map
      FloatMatrixWritable owValue = new FloatMatrixWritable(entry.getValue());
      // in the next rows:  	item  [tidList]  bucket_num
      mos.write(owKey, owValue, baseOutputPath);
    }
    //in the last row:		[]  []  bucket_num
    mos.write(EmptyIaw, EmptyImw, baseOutputPath);
  }
  
  /**
   * Check if exceeds the max number of tids
   */
  private static boolean checkLowestBucket(int lowestBucket, int totalTids) {
    return (lowestBucket + totalTids) <= MAX_NUMBER_OF_TIDS;
  }
  
  /**
   * Return the number of lowestBucket available 
   */
  private int getLowestBucket() {
    double min = Integer.MAX_VALUE;
    int id = -1;
    int ix = 0;
    for (MutableInt bucketSize : bucketSizes) {
      int bs = bucketSize.intValue();
      if (bs < min) {
        min = bs;
        id = ix;
      }
      ix++;
    }
    return id;
  }
  
}