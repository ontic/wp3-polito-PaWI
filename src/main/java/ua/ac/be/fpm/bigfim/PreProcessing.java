package ua.ac.be.fpm.bigfim;


import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.Text;

import ua.ac.be.fpm.eclat.util.WeightedItem;
 
 /**
  * Added class to preProcess the input of the map
  * Transform the row in a list of different transactions 
  */
 public class PreProcessing{
	 
  public static List<String> preProcess(Text value){
	// the format of value(row of the input file) is:
	// item:weight item:weight item:weight
	String delimeter1 = " ";
	String delimeter2 = ":";
	List<String> rows = new ArrayList<String>();
	// WeightedItem new class to represent id of item and his weight
	List<WeightedItem> itemList = new ArrayList<WeightedItem>();
	String line = value.toString();
	// split the row (each element is item:weight)
	String[] itemsSplit = line.split(delimeter1);
	for (String itemString : itemsSplit){
		// split each element in item and weight
		String[] elem = itemString.split(delimeter2);
		int itemId = Integer.parseInt(elem[0]);
		float weight = Float.parseFloat(elem[1]);
		// store the element in a WeightedItem variable
		WeightedItem wItem = new WeightedItem(itemId, weight);
		// add at list of WeightedItem
		itemList.add(wItem);
	}
	
	boolean fine = false;
	// print the list until it is empty
	while(fine == false){
		// Search the min weight between all the items of the transaction, excluding item with weight 0
		float min = takeMin(itemList);
		int[] arr = takeItems(itemList);
		// write the array in a unique string
		StringBuilder st = new StringBuilder();
		st.append(min).append("\t");
		int j;
		for(j = 0; j < arr.length - 1; j++){
			st.append(arr[j]).append(" ");
		}
		st.append(arr[j]);
		// add the string(new transaction) at the list
		rows.add(st.toString());
		
		// Subtract the min at all items != 0
		for(WeightedItem wItem : itemList){
			if(wItem.weight != 0){
				wItem.weight -= min;
			}
		}
		// return True if there isn't items with weight != 0
		fine = controlSize(itemList);
	}
	// return the list of strings(new transactions)
	return rows;
	
  }

/**
 * Control the number of item with weight != 0; True if all have weight = 0
 */
private static boolean controlSize(List<WeightedItem> itemList) {
	int num = 0;
	for(WeightedItem wList : itemList){
		if(wList.weight != 0){
			num++;
		}
	}
	boolean vuoto = false;
	if(num == 0){
		// no item with weight != 0
		vuoto = true;
	} else {
		vuoto = false;
	}
	return vuoto;
}



/**
 * Return the list of items with weight != 0
 */

  private static int[] takeItems(List<WeightedItem> itemList) {
	int size = 0;  
	// count how many items have weight != 0
	for(WeightedItem wItem : itemList){
		if(wItem.weight != 0){
			size++;			
		}
	}
	
	// initialize an array of "size" elements
	int[] arr = new int[size];
	int i = 0;
	// copy the items with weight != 0 in an array
	for(WeightedItem wItem : itemList){
		if(wItem.weight != 0){
			int id = wItem.itemId;
			arr[i] = id;
			i++;			
		}
	}
	return arr;
}

  
/**
 * Search the min weight between all the items of the transaction, excluding item with weight 0 
 */
private static float takeMin(List<WeightedItem> itemList) {
	float min = 9999999;
	for(WeightedItem wItem : itemList){
		float num = wItem.weight;
		if(num < min && num != 0){
			min = num;
		}
	}
	return min;
  }


}