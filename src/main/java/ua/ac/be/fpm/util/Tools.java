/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.util;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import ua.ac.be.fpm.eclat.util.TidList;

/**
 * Some extra utility functions for FPM algorithms
 */
public class Tools {

  public static TidList intersect(TidList tidList1, TidList tidList2) {
    float[][] intersection = new float[tidList1.tids.length][];
    for (int i = 0; i < tidList1.tids.length; i++) {
      float[] tids_1 = tidList1.tids[i];
      float[] tids_2 = tidList2.tids[i];

      if (tids_1 != null && tids_2 != null) {
        float[] inter = intersect(tids_1, tids_2);
        if (inter.length != 0) {
          intersection[i] = inter;
        }
      }
    }
    return new TidList(intersection);
  }

  public static TidList setDifference(TidList tidList1, TidList tidList2) {
    float[][] difference = new float[tidList1.tids.length][];
    for (int i = 0; i < tidList1.tids.length; i++) {
      float[] tids_1 = tidList1.tids[i];
      float[] tids_2 = tidList2.tids[i];

      if (tids_2 == null) {
        difference[i] = tids_1;
      } else if (tids_1 != null) {
	// se entrambe le tidList non sono vuote passo a setDifference(int, int), altra funzione
        final float[] diff = setDifference(tids_1, tids_2);
        if (diff.length != 0) {
          difference[i] = diff;
        }
      }
    }

    return new TidList(difference);
  }

  /**
   * Computes the intersection of two integer arrays and reports it.
   *
   * @param tids1
   *          the first array of integers
   * @param tids2
   *          the second array of integer
   * @return the intersection of the two integer arrays
   */
  private static float[] intersect(float[] tids1, float[] tids2) {
    List<Float> intersection = newArrayList();

    int ix1 = 0, ix2 = 0;
    while (ix1 != tids1.length && ix2 != tids2.length) {
      float i1 = tids1[ix1];
      float i2 = tids2[ix2];
      if (i1 == i2) {
        intersection.add(i1);
        ix1++;
        ix2++;
      } else if (i1 < i2) {
        ix1++;
      } else {
        ix2++;
      }
    }

    return toFloatArray(intersection);
  }

  /**
   * Computes the set difference of two integer arrays and reports it. Set difference is obtained by removing the
   * integers from the second array of integers from the first array of integers.
   * DA MODIFICARE
   *
   * @param tids1
   *          the first array of integers
   * @param tids2
   *          the second array of integer
   * @return the intersection of the two integer arrays
   */
  private static float[] setDifference(float[] tids1, float[] tids2) {
    List<Float> difference = newArrayList();
    // AGGIUNTA
    List<Integer> indexes = newArrayList();
    
    float[] t1 = new float[(tids1.length/2)];
    float[] t2 = new float[(tids2.length/2)];
    
    // RIEMPIO T1 CON I SOLI TIDS DI TIDS1(ESCLUDO I PESI)
    int j = 0;
    for(int i1 = 0; i1 < tids1.length; i1++){
    	if((i1%2) == 0){
    		t1[j] = tids1[i1];
    		j++;
    	}
    }
    // RIEMPIO T2 CON I SOLI TIDS DI TIDS2(ESCLUDO I PESI)
    int k = 0;
    for(int i2 = 0; i2 < tids2.length; i2++){
    	if((i2%2) == 0){
    		t2[k] = tids2[i2];
    		k++;
    	}
    }

    int ix1 = 0, ix2 = 0;
    boolean uguali;
    // DEVO MODIFICARE IL CONFRONTO PERCHE' GLI ARRAY NON SONO PIÙ ORDINATI
    for(ix1 = 0; ix1 < t1.length; ix1++){
    	uguali = false;
    	for(ix2 = 0; ix2 < t2.length; ix2++){
    		if(t1[ix1] == t2[ix2]){
    			uguali = true;
    		}
    	}
    	// SE NON SONO UGUALI AGGIUNGO L'INDICE ALLA LISTA INDEXES
    	if(uguali == false){
    		indexes.add(ix1);
    	}
    }
    // NELLA LISTA DIFFERENCE COPIO GLI INDICI RIMASTI PIU' I PESI ASSOCIATI
    for(int ind : indexes){
    	int ind1 = ind*2;
    	int ind2 = (ind*2)+1;
    	difference.add(tids1[ind1]);
    	difference.add(tids1[ind2]);
    }
    
    /*while (ix1 != tids1.length && ix2 != tids2.length) {
	      float i1 = tids1[ix1];
	      float i2 = tids2[ix2];
	      if (i1 == i2) {
	        ix1++;
	        ix2++;
	      } else if (i1 < i2) {
	        // memorizzo solo gli elementi presenti in tids1 e non in tids2 (le tids sono ordinate)
	        difference.add(tids1[ix1]);
	        ix1++;
	      } else {
	        ix2++;
	      }
    }
    // se tids2 é finito e tids1 ha ancora elementi in fondo li salvo in difference
    for (; ix1 < tids1.length; ix1++) {
      difference.add(tids1[ix1]);
    }*/

    return toFloatArray(difference);
  }

  private static float[] toFloatArray(List<Float> list) {
    float[] intArray = new float[list.size()];
    int ix = 0;
    for (Float i : list) {
      intArray[ix++] = i;
    }
    return intArray;
  }
}
