/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ua.ac.be.fpm.hadoop.util;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Writable;

/**
 * Provides easy access to IntArrayWritables
 */
public class FloatArrayWritable extends ArrayWritable {
  
  public final static FloatArrayWritable EmptyIaw = new FloatArrayWritable(new FloatWritable[0]);
  
  public static FloatArrayWritable of(float[] tids) {
    if (tids == null) {
      return EmptyIaw;
    }
    FloatWritable[] iw = new FloatWritable[tids.length];
    for (int i = 0; i < iw.length; i++) {
      iw[i] = new FloatWritable(tids[i]);
    }
    return new FloatArrayWritable(iw);
  }
  
  public static FloatArrayWritable of(Float i) {
    FloatWritable[] iw = new FloatWritable[1];
    iw[0] = new FloatWritable(i);
    return new FloatArrayWritable(iw);
  }
  
  public static FloatArrayWritable of(String string) {
    String[] splits = string.split(" ");
    FloatWritable[] iw = new FloatWritable[splits.length];
    int i = 0;
    for (String split : splits) {
      iw[i++] = new FloatWritable(Integer.parseInt(split));
    }
    return new FloatArrayWritable(iw);
  }
  
  public FloatArrayWritable() {
    super(FloatWritable.class);
  }
  
  public FloatArrayWritable(FloatWritable[] floatWritables) {
    this();
    set(floatWritables);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (String s : super.toStrings()) {
      sb.append(s).append(" ");
    }
    return sb.toString();
  }
  
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    FloatArrayWritable other = (FloatArrayWritable) obj;
    Writable[] iw1 = this.get();
    Writable[] iw2 = other.get();
    if (iw1.length != iw2.length) return false;
    for (int i = 0; i < iw1.length; i++)
      if (!iw1[i].equals(iw2[i])) return false;
    return true;
  }
  
  @Override
  public int hashCode() {
    int hashCode = 0;
    for (Writable i : get()) {
      hashCode += i.hashCode();
    }
    return hashCode;
  }
}