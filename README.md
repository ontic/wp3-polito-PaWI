PaWI
=======
PaWI (Parallel Weighted Itemset miner) is a weighted itemset mining algorithm based on the MapReduce paradigm. 
The traditional (unweighted) itemset minig problem allows mining useful and interesting correlations among set of items. 
However, in many real applications items are unlikely to be equally important. For example, items purchased at the market have different 
prices and genes are expressed in biological samples with different levels of significance. In the network domain, network features can
have different importance/weight depending on the analysis the domain expert is performing.
These weights are important and must be considered in order to mine interesting weighted itemsets.

The code avaiable in this repository is the implementation of the PaWI algorithm described in the paper:
Elena Baralis, Luca Cagliero, Paolo Garza, and Luigi Grimaudo. PaWI: Parallel Weighted Itemset Mining by Means of MapReduce. IEEE Congress on Big Data (BigData Congress 2015), June 27-July 2, 2015, New York, pp. 25-32, DOI 10.1109/BigDataCongress.2015.14, available at http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7207198

Usage
=======

## Compile

The project uses maven to compile & build the PaWI tool. The following command downloads the dependencies, compiles the code, and creates the required jar file in the `target` directory.

	mvn package

The generated jar file, contaning PaWI, is PaWI-0.1-jar-with-dependencies.jar 

The jar file PaWI-0.1-jar-with-dependencies.jar includes also all the needed dependencies. Hence, no libraries need to be uploaded on the Hadoop cluster in order to run PaWI.

Please note that the implementation is developed and tested on 2.5.0-cdh5.3.1 Cloudera configuration.

## Input dataset format

The PaWI algorithm mines frequent weigthed itemsets from a dataset of weighted transactions. Each weighted transaction is a set of pairs (item, weight). 
PaWI works on datasets stored by means of textual files. Specifically, a dataset is a textual file where each row of the file is a weighted transaction. Each transaction is a list of pairs <item:weight> separated by spaces, where item is an integer (the id of the item) and weight is a real number (the weight of item in the current transaction).
The format is the same of the example file "example_pawi.txt" available in the repository. 
For example,
139:18 155:29 262:22 717:21
is the representation of a weighted transaction composed of the four items 139, 155, 262, and 717 and their weights in the transaction are 18, 29, 22, and 21, respectively.

## Run the algorithm

The following command runs the algorithm:

	hadoop jar pawi-0.1.jar it.polito.dbdmg.pawi.PaWIDriver -i <input_dataset> -o <output_folder> -s <minimum_weighted_support> -m <number_of_mappers> -p <prefix_length>

The mined frequent itemsets are stored in the hdfs directory `<output_directory>/fis/`.

PaWI has the following mandatory parameters:
- input_dataset: name of the file that contains the weighted transactions to analyze

- output_folder: name of the HDFS folder in which the mined weighted itemsets will be stored (the mined itemsets will be stored in Output folder/fis)

- minimum_weighted_support: the minimum absolute weighted support threshold. A weighted itemset is extracted if and only if its  weighted support is greater than or equal to Weighted minsup.

- number_of_mappers: the number of mappers involved in the computation. 

- prefix_length: the length of the itemsets mined by the initial Apriori phase of PaWI. Higher values of it allows obtaining a more balanced execution of the program (suggested value: 2).


## Example of use:

The script `run.sh`, which is available in the repository, automatically uploads the example dataset example_pawi.txt in HDFS and runs PaWI on it.

	source run.sh

The mined weighted itemsets can be retrived by executing the following command:
	
	hdfs dfs -cat outputTestPaWI/fis/part*