# Copy the example dataset in HDFS
hdfs dfs -put example_pawi.txt

# Run PaWI
# input dataset example_pawi.txt
# output folder outputTestPaWI
# minimum weighted support threshold 100
# number of mappers 8 
# prefix length 2
hadoop jar target/PaWI-0.1-jar-with-dependencies.jar it.polito.dbdmg.pawi.PaWIDriver -i example_pawi.txt -o outputTestPaWI -s 100 -m 8 -p 2


# The mined frequent itemsets can be retrived by executing the command:
# hdfs dfs -cat outputTestPaWI/fis/part*

